$("#upArrow1").click(function(e) {
    // Do something
    e.stopPropagation();
    scrollToTop();
});

$("#upArrow2").click(function(e) {
    // Do something
    e.stopPropagation();
    scrollToSlide("first-slide");
});

$("#upArrow3").click(function(e) {
    // Do something
    e.stopPropagation();
    scrollToSlide("second-slide");
});

$("#upArrow4").click(function(e) {
    // Do something
    e.stopPropagation();
    scrollToSlide("third-slide");
});

$("#upArrow5").click(function(e) {
    // Do something
    e.stopPropagation();
    scrollToSlide("fourth-slide");
});

function scrollToTop() {
    scrollToSlide("intro-container");
}

function scrollToSlide(id) {
    var element = document.getElementById(id);
    element.scrollIntoView();
}

window.addEventListener("load", scrollToTop);
document.addEventListener("load", scrollToTop);